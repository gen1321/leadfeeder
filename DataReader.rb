require 'csv'
require 'redis'
class DataReader


  def self.read(filename)
    csv = read_from_csv(filename)
    csv = cut_csv(csv)
    csv = clean_up(csv)
    start_and_end = add_start_and_end(csv)
    upload_to_DB(csv , start_and_end[:start], start_and_end[:end] )
  end

  def self.read_from_csv(file)
    csv = CSV.read(file)
  end

  def self.cut_csv(csv)
    csv.map.each do |arr|
      if arr[0] != nil
        y, m, d = arr[0].split '-'
        if (Date.valid_date? y.to_i, m.to_i, d.to_i)
          arr
        end
      end
    end
  end

  def self.clean_up(csv)
    csv = csv.compact
    csv.map.each do |d|
      d.compact
    end
  end


  def self.add_start_and_end(csv)
    date_start = csv[csv.size-1][0]
    date_end   = csv[0][0]
    {start:date_start , end:date_end }
  end


  def self.upload_to_DB(csv,date_start,date_end)

    redis = Redis.new
    redis.set('date_start',date_start)
    redis.set('date_end',date_end)

    csv.each do |arr|
      redis.set(arr[0], arr[1])
    end

  end

end
