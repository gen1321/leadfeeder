require 'rspec'
require_relative '../DataReader'
require 'redis'

describe DataReader do
  let(:csv){DataReader.read_from_csv('data.csv')}
  let(:cuted_csv){DataReader.cut_csv(csv) }
  let(:clean_csv){DataReader.clean_up(cuted_csv)}
  let(:start_and_end){DataReader.add_start_and_end(clean_csv)}

  it '#load csv to db' do
    redis = Redis.new
    DataReader.read('data.csv')
    expect(redis.get('2012-10-26')).to eq('1.2908')
  end

  it '#read_from_csv' do
    csv = DataReader.read_from_csv('data.csv')
    expect(csv).to_not eq(nil)
  end

  it '#cut_csv' do
    DataReader.cut_csv(csv)
  end

  it '#clean_up' do
   d =  DataReader.clean_up(cuted_csv)
   expect(d[0]).to eq(["2016-04-29", "1.1403"])
  end

  it '#add_stat_and_end dates ' do
    start_and_end = DataReader.add_start_and_end(clean_csv)
    expect(start_and_end[:start]).to eq('1999-01-04')
    expect(start_and_end[:end]).to eq('2016-04-29')
  end

  it '#upload_to_DB' do
    redis = Redis.new
    DataReader.upload_to_DB(clean_csv,start_and_end[:start],start_and_end[:end])
    expect(redis.get('2012-10-26')).to eq('1.2908')
    expect(redis.get('date_start')).to eq('1999-01-04')
    expect(redis.get('date_end')).to eq('2016-04-29')
  end



end