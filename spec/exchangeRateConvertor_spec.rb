require 'rspec'
require_relative '../exchangeRateConvertor'

describe ExchangeRateConvertor do
let(:work_day){'2016-04-29'}
let(:holiday){'2016-04-24'}
let(:tommorow){ (Date.parse(Time.now.to_s) + 1).to_s }
  context 'date is work day' do
    it 'convert dollar to eur' do
     expect(ExchangeRateConvertor.convert(1,work_day)).to eq(0.88)
    end
  end
  context 'date is holiday' do
    it 'convert dollar to eur with previous available rates' do
      expect(ExchangeRateConvertor.convert(1,holiday)).to eq(0.89)
    end
  end

  context 'when Date is invalid' do
   it 'rise error' do
    expect{ExchangeRateConvertor.convert(1,tommorow)}.to raise_error(RuntimeError)
   end
  end
end