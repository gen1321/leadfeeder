require 'redis'
class ExchangeRateConvertor

  def self.convert(amount, date)
    amount = amount.to_f
    redis = Redis.new
    check_date(date, redis)
    rate = get_rate(date, redis).to_f
    (amount / rate).round 2
  end


  def self.get_rate(date, redis)
    rate = redis.get(date).to_f
    while rate == 0.0
      date = (Date.parse(date) - 1).to_s
      rate = redis.get(date).to_f
    end
    return rate
  end

  def self.check_date(date, redis)
    start_date = Date.parse(redis.get('date_start'))
    end_date   = Date.parse(redis.get('date_end'))
    date       = Date.parse(date)
    if (date < start_date) || (date > end_date)
      raise "We do not have rate for this date"
    end
  end

end